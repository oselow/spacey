package com.example.spacey.apiFeat.remote

import com.example.spacey.apiFeat.model.AnimalRetrofit
import retrofit2.http.GET

interface AnimalEndPoint {

    @GET("/cat?json=true")
    suspend fun getRandom() : AnimalRetrofit
}