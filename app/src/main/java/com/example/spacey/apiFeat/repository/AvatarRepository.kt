package com.example.spacey.apiFeat.repository

import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.spacey.apiFeat.model.AvatarRetrofit
import com.example.spacey.apiFeat.model.AvatarRoom
import com.example.spacey.apiFeat.model.AvatarUi
import com.example.spacey.architecture.CustomApplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit

class AvatarRepository {
    private val mAvatarDao = CustomApplication.instance.mApplicationDatabase.avatarDao()

    fun selectAllAvatar(): LiveData<List<AvatarRoom>> {
        return mAvatarDao.selectAll()
    }


    private suspend fun insertAvatar(avatar: AvatarRoom) =
        withContext(Dispatchers.IO) {
            mAvatarDao.insert(avatar)
        }


    suspend fun deleteAllAvatar() = withContext(Dispatchers.IO) {
        mAvatarDao.deleteAll()
    }

    suspend fun deleteAvatar(avatar: AvatarUi) = withContext(Dispatchers.IO) {
        mAvatarDao.delete(avatar.id)

    }

    suspend fun fetchData() {
        insertAvatar(RetrofitBuilder.getAvatar().getRandomAvatar().toRoom())
    }
}


private fun AvatarRetrofit.toRoom(): AvatarRoom {
    return AvatarRoom(
        firstName = firstName,
        lastName = lastName,
        avatarUrl = avatarUrl,
        gender = gender,
        username = username,
        timestamp = System.currentTimeMillis()
    )
}
