package com.example.spacey.apiFeat.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.spacey.apiFeat.model.AnimalUi
import com.example.spacey.apiFeat.model.AvatarUi
import com.example.spacey.apiFeat.model.EntityForRecyclerView
import com.example.spacey.apiFeat.model.EntityHeaderSample
import com.example.spacey.databinding.EntityCustomRecyclerHeaderBinding
import com.example.spacey.databinding.ItemAnimalBinding
import com.example.spacey.databinding.ItemAvatarBinding

private val diffItemUtils = object : DiffUtil.ItemCallback<EntityForRecyclerView>() {

    override fun areItemsTheSame(
        oldItem: EntityForRecyclerView,
        newItem: EntityForRecyclerView
    ): Boolean {
        return oldItem == newItem
    }


    override fun areContentsTheSame(
        oldItem: EntityForRecyclerView,
        newItem: EntityForRecyclerView
    ): Boolean {
        return oldItem == newItem
    }
}

class AvatarViewHolder(
    private val binding: ItemAvatarBinding,
    onItemClick: (avatarSample: AvatarUi, view: View) -> Unit,
    onDeleteClick:(avatarSample: AvatarUi, view: View) -> Unit,
) : RecyclerView.ViewHolder(binding.root) {

    private lateinit var ui: AvatarUi

    init {
        binding.root.setOnClickListener {
            onItemClick(ui, itemView)
        }
        binding.card.setOnLongClickListener {
            binding.card.setChecked(!binding.card.isChecked)
            true
        }
        binding.imageDelete.setOnClickListener {
          onDeleteClick(ui, itemView)
        }
    }


    fun bind(avatarUi: AvatarUi) {
        ui = avatarUi
        Glide.with(itemView.context)
            .load(avatarUi.avatarUrl)
            .into(binding.itemAvatarIcon)


        binding.itemAvatarFirstName.text = avatarUi.firstName
        binding.itemAvatarLastName.text = avatarUi.lastName
        binding.itemAvatarUsername.text = avatarUi.username
        binding.itemAvatarGender.text = avatarUi.gender

    }


}


class AnimalViewHolder(
    private val binding: ItemAnimalBinding,
    onItemClick: (animalSample: AnimalUi, view: View) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    private lateinit var ui: AnimalUi

    init {
        binding.root.setOnClickListener{
            onItemClick(ui, itemView)
        }
    }

    fun bind(animalUi: AnimalUi) {
        ui = animalUi
        Glide.with(itemView.context)
            .load(animalUi.iconUrl)
            .into(binding.itemAnimalIcon)


        binding.itemAnimal.text = animalUi.date1
    }
}

class EntityHeaderViewHolder(
    private val binding: EntityCustomRecyclerHeaderBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(personDataFooterSample: EntityHeaderSample) {
        binding.itemRecyclerViewHeader.text = personDataFooterSample.header
    }
}

class ApiAdapter(
    private val onAvatarClick: (avatarUi: AvatarUi, view: View) -> Unit,
    private val onAnimalClick: (animalUi: AnimalUi, view: View) -> Unit,
    private val onDeleteAvatarClick: (avatarUi: AvatarUi, view: View) -> Unit,

) :
    ListAdapter<EntityForRecyclerView, RecyclerView.ViewHolder>(diffItemUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            MyItemType.AVATAR.type -> {
                AvatarViewHolder(
                    ItemAvatarBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ), onAvatarClick ,onDeleteAvatarClick
                )
            }

            MyItemType.ANIMAL.type -> {
                AnimalViewHolder(
                    ItemAnimalBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ), onAnimalClick
                )
            }


            MyItemType.HEADER.type -> {
                EntityHeaderViewHolder(
                    EntityCustomRecyclerHeaderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }

            else -> throw RuntimeException("Wrong view type received $viewType")
        }
//        return AvatarViewHolder(
//            ItemAvatarBinding.inflate(
//                LayoutInflater.from(parent.context),
//                parent,
//                false
//            ), onItemClick
//        )

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is AvatarUi -> MyItemType.AVATAR.type
            is AnimalUi -> MyItemType.ANIMAL.type
            is EntityHeaderSample -> MyItemType.HEADER.type
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =

        when (holder.itemViewType) {
            MyItemType.AVATAR.type -> (holder as AvatarViewHolder).bind(getItem(position)  as AvatarUi)

            MyItemType.ANIMAL.type -> (holder as AnimalViewHolder).bind(getItem(position) as AnimalUi)

            MyItemType.HEADER.type -> (holder as EntityHeaderViewHolder).bind(
                getItem(
                    position
                ) as EntityHeaderSample
            )

            else -> throw RuntimeException("Wrong view type received ${holder.itemView}")
        }

}


enum class MyItemType(val type: Int) {
    AVATAR(0),
    ANIMAL(1),
    HEADER(2)
}

