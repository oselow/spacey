package com.example.spacey.apiFeat.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** Object use for room */
@Entity(tableName = "avatars")
data class AvatarRoom(
    @ColumnInfo(name = "first_name")
    val firstName: String,

    @ColumnInfo(name = "last_name")
    val lastName: String,

    @ColumnInfo(name = "avatar")
    val avatarUrl: String,

    @ColumnInfo(name = "gender")
    val gender: String,

    @ColumnInfo(name = "username")
    val username: String,

    @ColumnInfo(name = "timestamp")
    val timestamp: Long
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}

/** Object use for Ui */
data class AvatarUi(
    val firstName: String,
    val lastName: String,
    val avatarUrl: String,
    val gender: String,
    val username: String,
    override val timestamp: Long,
    val id: Long
): EntityForRecyclerView()

/** Object use for retrofit */
data class AvatarRetrofit(
    @Expose
    @SerializedName("first_name")
    val firstName: String,

    @Expose
    @SerializedName("last_name")
    val lastName: String,

    @Expose
    @SerializedName("avatar")
    val avatarUrl: String,

    @Expose
    @SerializedName("gender")
    val gender: String,

    @Expose
    @SerializedName("username")
    val username: String,

    @Expose
    @SerializedName("timestamp")
    val timestamp: Long
)