package com.example.spacey.apiFeat.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.spacey.apiFeat.model.AnimalRoom


@Dao
interface AnimalDao {


    @Query("SELECT * FROM animals")
    fun selectAll() : LiveData<List<AnimalRoom>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(animalRoom: AnimalRoom)


    @Query("DELETE FROM animals")
    fun deleteAll()
}

