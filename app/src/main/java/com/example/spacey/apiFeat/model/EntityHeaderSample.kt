package com.example.spacey.apiFeat.model

import java.sql.Timestamp

sealed class EntityForRecyclerView() {
    abstract val timestamp: Long
}

data class EntityHeaderSample(
    val header: String
): EntityForRecyclerView() {
    override val timestamp: Long
        get() = TODO("Not yet implemented")
}