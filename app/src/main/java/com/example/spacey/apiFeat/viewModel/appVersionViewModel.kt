package com.example.spacey.apiFeat.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.example.spacey.apiFeat.model.AnimalRoom
import com.example.spacey.apiFeat.model.AnimalUi
import com.example.spacey.apiFeat.model.AvatarRoom
import com.example.spacey.apiFeat.model.AvatarUi
import com.example.spacey.apiFeat.repository.AnimalQuoteRepository
import com.example.spacey.apiFeat.repository.AvatarRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ApiViewModel : ViewModel() {

    private val mAvatarRepository: AvatarRepository by lazy { AvatarRepository() }
    var mAvatarLiveData: LiveData<List<AvatarUi>> =
        mAvatarRepository.selectAllAvatar().map {
            it.toUi()
        }

    fun fetchNewAvatar() {
        viewModelScope.launch(Dispatchers.IO) {
            mAvatarRepository.fetchData()
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            mAvatarRepository.deleteAllAvatar()
            mAnimalRepository.deleteAllAnimal()
        }
    }

    fun delete(avatarUi: AvatarUi) {
        viewModelScope.launch(Dispatchers.IO) {
            mAvatarRepository.deleteAvatar(avatarUi)
       }
    }


    //ANIMAUX
    private val mAnimalRepository: AnimalQuoteRepository by lazy { AnimalQuoteRepository() }
    var mAnimalLiveData: LiveData<List<AnimalUi>> =
        mAnimalRepository.selectAllAnimal().map {
            it.toUi()
        }


    fun fetchNewQuote() {
        viewModelScope.launch(Dispatchers.IO) {
            mAnimalRepository.fetchData()
        }
    }
}

@JvmName("toUiAvatarRoom")
private fun List<AvatarRoom>.toUi(): List<AvatarUi> {
    return asSequence().map {
        AvatarUi(
            firstName = it.firstName,
            lastName = it.lastName,
            avatarUrl = it.avatarUrl,
            gender = it.gender,
            username = it.username,
            timestamp = it.timestamp,
            id = it.id
        )
    }.toList()
}

@JvmName("toUiAnimalRoom")
private fun List<AnimalRoom>.toUi(): List<AnimalUi> {
    return asSequence().map {
        AnimalUi(
            date1 = it.date1,
            iconUrl = it.iconUrl,
            timestamp = it.timestamp
        )
    }.toList()
}
