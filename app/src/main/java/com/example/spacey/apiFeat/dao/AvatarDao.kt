package com.example.spacey.apiFeat.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.spacey.apiFeat.model.AvatarRoom
import com.example.spacey.apiFeat.model.AvatarUi

@Dao
interface AvatarDao {


    @Query("SELECT * FROM avatars")
    fun selectAll() : LiveData<List<AvatarRoom>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(avatarRoom: AvatarRoom)


    @Query("DELETE FROM avatars")
    fun deleteAll()

    @Query("DELETE FROM avatars WHERE id = :id")
    fun delete(id: Long)
}
