package com.example.spacey.apiFeat.repository

import androidx.lifecycle.LiveData
import com.example.spacey.apiFeat.model.AnimalRetrofit
import com.example.spacey.apiFeat.model.AnimalRoom
import com.example.spacey.architecture.CustomApplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class AnimalQuoteRepository {
    private val mAnimalDao = CustomApplication.instance.mApplicationDatabase.animalDao()

    fun selectAllAnimal(): LiveData<List<AnimalRoom>> {
        return mAnimalDao.selectAll()
    }


    private suspend fun insertAnimal(animalQuote: AnimalRoom) =
        withContext(Dispatchers.IO) {
            mAnimalDao.insert(animalQuote)
        }


    suspend fun deleteAllAnimal() = withContext(Dispatchers.IO) {
        mAnimalDao.deleteAll()
    }


    suspend fun fetchData() {
        insertAnimal(RetrofitBuilder.getAnimal().getRandom().toRoom())
    }
}


private fun AnimalRetrofit.toRoom(): AnimalRoom {
    return AnimalRoom(
        date1 = "crée le : " +date1,
        iconUrl = "https://cataas.com/"+iconUrl,
        timestamp = System.currentTimeMillis()
    )
}




