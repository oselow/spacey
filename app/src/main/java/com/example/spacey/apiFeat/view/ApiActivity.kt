package com.example.spacey.apiFeat.view

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.spacey.R
import com.example.spacey.apiFeat.model.AnimalUi
import com.example.spacey.apiFeat.model.AvatarUi
import com.example.spacey.apiFeat.model.EntityForRecyclerView
import com.example.spacey.apiFeat.model.EntityHeaderSample
import com.example.spacey.apiFeat.viewModel.ApiViewModel
import com.example.spacey.databinding.ActivityApiBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random.Default.nextBoolean

class ApiActivity : AppCompatActivity(){
    private lateinit var apiViewModel: ApiViewModel
    private lateinit var binding : ActivityApiBinding
    private lateinit var apiAdapter : ApiAdapter

    private val avatarObserver = Observer<List<AvatarUi>> { avatars ->
        update(avatars)
    }

    private val animalObserver = Observer<List<AnimalUi>> { animals ->
        update(animals)
    }

    /*private fun removeItem(position: Int) {
        val newPosition: Int = holder.getAdapterPosition()
        model.remove(newPosition)
        notifyItemRemoved(newPosition)
        notifyItemRangeChanged(newPosition, model.size())
    }*/
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "com.example.spacey"
    private val description = "api notifications"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityApiBinding.inflate(layoutInflater)
        setContentView(binding.root)
        apiAdapter = ApiAdapter(
            { item, view ->
                onAvatarClick(item, view)
            },
            { item, view ->
                onAnimalClick(item, view)
            })
            { item, view ->
                onDeleteAvatarClick(item, view)
            }

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        apiViewModel = ViewModelProvider(this)[ApiViewModel::class.java]

        binding.avatarActivityRv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.avatarActivityRv.adapter = apiAdapter

        binding.avatarActivityAdd.setOnClickListener {
            var rand = nextBoolean()
            if (rand)
                apiViewModel.fetchNewAvatar()
            else
                apiViewModel.fetchNewQuote()
//            apiViewModel.fetchNewAvatar()
        }

        binding.avatarActivityDelete.setOnClickListener {
            apiViewModel.deleteAll()
        }
    }
    override fun onStart() {
        super.onStart()
        apiViewModel.mAvatarLiveData.observe(this, avatarObserver)
        apiViewModel.mAnimalLiveData.observe(this, animalObserver)
    }



    override fun onStop() {
        apiViewModel.mAvatarLiveData.removeObserver(avatarObserver)
        apiViewModel.mAnimalLiveData.removeObserver(animalObserver)
        super.onStop()
    }

    private fun onAvatarClick(avatar: AvatarUi, view : View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
//        Toast.makeText(this, person.lastName + " " + person.firstName, Toast.LENGTH_LONG).show()

        val intent = Intent(this, ApiActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)

        notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
        notificationChannel.enableLights(true)
        notificationChannel.lightColor = Color.GREEN
        notificationChannel.enableVibration(false)
        notificationManager.createNotificationChannel(notificationChannel)

        builder = Notification.Builder(this, channelId)
            .setContentTitle("Avatar")
            .setContentText(avatar.lastName + " " + avatar.firstName)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.mipmap.ic_launcher))
            .setContentIntent(pendingIntent)

        notificationManager.notify(1234,builder.build())
    }

    private fun onAnimalClick(animal: AnimalUi, view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)

        val intent = Intent(this, ApiActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)

        notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
        notificationChannel.enableLights(true)
        notificationChannel.lightColor = Color.GREEN
        notificationChannel.enableVibration(false)
        notificationManager.createNotificationChannel(notificationChannel)

        builder = Notification.Builder(this, channelId)
            .setContentTitle("Animal")
            .setContentText(animal.date1)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.mipmap.ic_launcher))
            .setContentIntent(pendingIntent)

        notificationManager.notify(1234,builder.build())
    }

    private fun onDeleteAvatarClick(ui: AvatarUi, view: View) {

        Toast.makeText(this, ui.toString(), Toast.LENGTH_LONG).show()
        apiViewModel.delete(ui)
    }

    private fun update(base: List<EntityForRecyclerView>){
        val result = mutableListOf<EntityForRecyclerView>()
        val data = mutableListOf<EntityForRecyclerView>()

        //  Ordre des add important pour l'ordre d'apparition
        if (base.isNotEmpty() && base.first() is AvatarUi){
            data.addAll(base)
            apiViewModel.mAnimalLiveData.value?.let { data.addAll(it) }
        }else if (base.isNotEmpty()){
            apiViewModel.mAvatarLiveData.value?.let { data.addAll(it) }
            data.addAll(base)
        }else{
            data.addAll(base)
        }

        data.groupBy {
            SimpleDateFormat("dd-MM-YYYY HH:mm").format(Date(it.timestamp)).toString()
        }.forEach { (timestamp, items) ->
            result.add(EntityHeaderSample("Date : ${timestamp}"))
            result.addAll(items)
        }

        apiAdapter.submitList(result)
    }
}