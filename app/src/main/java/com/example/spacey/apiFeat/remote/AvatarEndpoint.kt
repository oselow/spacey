import com.example.spacey.apiFeat.model.AvatarRetrofit
import retrofit2.http.GET

interface AvatarEndpoint {
    @GET("users/random_user")
    suspend fun getRandomAvatar() : AvatarRetrofit
}
