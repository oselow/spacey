package com.example.spacey.apiFeat.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** Object use for room */
@Entity(tableName = "animals")
data class AnimalRoom(
    @ColumnInfo(name = "text")
    val date1: String,

    @ColumnInfo(name = "icon_url")
    val iconUrl: String,

    @ColumnInfo(name = "timestamp")
    val timestamp: Long
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}


/** Object use for Ui */
data class AnimalUi(
    val date1: String,
    val iconUrl: String,
    override val timestamp: Long
) : EntityForRecyclerView()

data class AnimalRetrofit(

    @Expose
    @SerializedName("created_at")
    val date1: String,

    @Expose
    @SerializedName("url")
    val iconUrl: String,

    @Expose
    @SerializedName("timestamp")
    val timestamp: Long
)
