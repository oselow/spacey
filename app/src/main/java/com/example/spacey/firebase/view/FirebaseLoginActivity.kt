package com.example.spacey.firebase.view

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.spacey.R
import com.example.spacey.databinding.ActivityFirebaseLoginBinding
import com.example.spacey.firebase.viewmodel.FirebaseAuthViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseUser
import java.util.regex.Pattern

class FirebaseLoginActivity : AppCompatActivity() {


    private lateinit var mViewModel: FirebaseAuthViewModel
    private lateinit var binding: ActivityFirebaseLoginBinding


    private lateinit var firebaseButtonRegister: Button
    private lateinit var firebaseButtonLogin: Button
    private lateinit var firebaseButtonDisconnect: Button
    private lateinit var firebaseUserEmail: EditText
    private lateinit var firebaseUserPassword: EditText
    private lateinit var firebaseUserConfirmPassword: EditText
    private lateinit var firebaseError: TextView
    private lateinit var firebaseLog: TextView
    private lateinit var firebaseSwitchMode: TextView

    private lateinit var mCurrentUser: MutableLiveData<FirebaseUser>

    private var isLoginMode: Boolean = true


    private var mObserverUser = Observer<FirebaseUser> {
        updateUser(it)
    }


    private var mObserverError = Observer<Int> {
        updateError(it)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFirebaseLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firebaseButtonRegister = binding.authButtonRegister
        firebaseButtonLogin = binding.authButtonLogin
        firebaseButtonDisconnect = binding.authButtonDisconnect

        firebaseUserEmail = binding.authEditTextEmail
        firebaseUserPassword = binding.authEditTextPassword
        firebaseUserConfirmPassword = binding.authEditTextPasswordConfirm
        firebaseError = binding.authTextViewError
        firebaseLog = binding.authTextViewLog
        firebaseSwitchMode = binding.authTextViewSwitchMode

        mViewModel = ViewModelProvider(this)[FirebaseAuthViewModel::class.java]
        mCurrentUser = mViewModel.mCurrentUser

        firebaseButtonRegister.setOnClickListener { register() }
        firebaseButtonLogin.setOnClickListener { login() }
        firebaseButtonDisconnect.setOnClickListener { disconnect() }
        firebaseSwitchMode.setOnClickListener{ this.switchMode(!this.isLoginMode) }

        this.updateButtonVisibility()
    }

    override fun onStart() {
        super.onStart()
        mViewModel.mCurrentUser.observe(this, mObserverUser)
        mViewModel.mErrorProcess.observe(this, mObserverError)
    }


    override fun onStop() {
        mViewModel.mCurrentUser.removeObserver(mObserverUser)
        mViewModel.mErrorProcess.removeObserver(mObserverError)
        super.onStop()
    }


    private fun checkConformityFields(): Boolean {
        if(!firebaseUserEmail.text.toString().isEmailValid()){
            firebaseError.text = getString(R.string.auth_invalidEmail_error)
            firebaseUserEmail.error = getString(R.string.auth_invalidEmail_error)
            return false
        }

        if(!firebaseUserPassword.text.toString().isPasswordValid()){
            firebaseError.text = getString(R.string.auth_invalidPassword_error)
            firebaseUserPassword.error = getString(R.string.auth_invalidPassword_error_details)
            return false
        }

        if(!isLoginMode && !firebaseUserConfirmPassword.text.toString().equals(firebaseUserPassword.text.toString())){
            firebaseError.text = getString(R.string.auth_invalidPassword_error)
            firebaseUserConfirmPassword.error = getString(R.string.auth_invalidConfirmPassword_error)
            return false
        }

        return true
    }

    private fun login() {
        firebaseError.text = getString(R.string.auth_loginProgress_status)
        if (checkConformityFields()) {
            mViewModel.loginUser(
                firebaseUserEmail.text.toString(),
                firebaseUserPassword.text.toString()
            )
        }
    }


    private fun register() {
        firebaseError.text = getString(R.string.auth_registerProgress_status)
        if (checkConformityFields()) {
            mViewModel.registerNewUser(
                firebaseUserEmail.text.toString(),
                firebaseUserPassword.text.toString()
            )
        }
    }

    private fun disconnect() {
        mViewModel.disconnectUser()
        this.updateButtonVisibility(false)
        Toast.makeText(this, getString(R.string.auth_disconnected_status_toast), Toast.LENGTH_LONG).show()
    }


    private fun updateUser(user: FirebaseUser) {
        mCurrentUser = mViewModel.mCurrentUser
        user.let {
            firebaseLog.text = "${user.uid}-${user.email}"
        }
        Toast.makeText(this, getString(R.string.auth_connected_status_toast), Toast.LENGTH_LONG).show()
        this.updateButtonVisibility()
    }

    private fun updateButtonVisibility(logoutMode: Boolean = true){
        if (this.mCurrentUser.value === null || !logoutMode){
            firebaseButtonDisconnect.visibility = View.INVISIBLE
            firebaseUserEmail.visibility = View.VISIBLE
            firebaseUserPassword.visibility = View.VISIBLE
            firebaseUserConfirmPassword.visibility = View.VISIBLE
            firebaseButtonLogin.visibility = View.VISIBLE
            firebaseButtonRegister.visibility = View.VISIBLE
            firebaseSwitchMode.visibility = View.VISIBLE
            this.switchMode(true)
        }else{
            firebaseButtonDisconnect.visibility = View.VISIBLE
            firebaseButtonLogin.visibility = View.INVISIBLE
            firebaseButtonRegister.visibility = View.INVISIBLE
            firebaseUserEmail.visibility = View.INVISIBLE
            firebaseUserPassword.visibility = View.INVISIBLE
            firebaseUserConfirmPassword.visibility = View.INVISIBLE
            firebaseSwitchMode.visibility = View.INVISIBLE
        }
    }

    private fun updateError(code: Int) {
        when (code) {
            5 -> {
                firebaseError.text = getString(R.string.auth_disconnected_status)
                firebaseLog.text = getString(R.string.auth_none_status)
            }
            9 -> firebaseError.text = getString(R.string.auth_nullUser_status)
            10 -> firebaseError.text = getString(R.string.auth_registerError_status)
            11 -> firebaseError.text = getString(R.string.auth_loginError_status)
            else -> firebaseError.text = getString(R.string.auth_allIsGood_status)
        }
    }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    fun String.isPasswordValid(): Boolean {
        val passwordREGEX = Pattern.compile("^" +
                "(?=.*[0-9])" +         //at least 1 digit
                "(?=.*[A-Z])" +         //at least 1 upper case letter
                "(?=.*[a-zA-Z])" +      //any letter
                "(?=\\S+$)" +           //no white spaces
                ".{8,}" +               //at least 8 characters
                "$")

        return !TextUtils.isEmpty(this) && passwordREGEX.matcher(this).matches()
    }

    fun switchMode(isLoginMode: Boolean){
        this.isLoginMode = isLoginMode

        if(isLoginMode){
            firebaseButtonRegister.visibility = View.INVISIBLE
            firebaseButtonLogin.visibility = View.VISIBLE
            firebaseUserConfirmPassword.visibility = View.INVISIBLE
            firebaseSwitchMode.text = getString(R.string.auth_notAlreadyRegistered_textView)
            return
        }

        firebaseButtonRegister.visibility = View.VISIBLE
        firebaseButtonLogin.visibility = View.INVISIBLE
        firebaseUserConfirmPassword.visibility = View.VISIBLE
        firebaseSwitchMode.text = getString(R.string.auth_alreadyRegistered_textView)
        return
    }
}

