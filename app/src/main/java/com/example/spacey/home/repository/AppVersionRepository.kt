package com.example.spacey.home.repository

import androidx.lifecycle.LiveData
import com.example.spacey.BuildConfig
import com.example.spacey.apiFeat.model.AnimalRetrofit
import com.example.spacey.apiFeat.model.AnimalRoom
import com.example.spacey.architecture.CustomApplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class AppVersionRepository {
    fun getAppVersion(): String {
        return BuildConfig.VERSION_CODE.toString()
    }
}




