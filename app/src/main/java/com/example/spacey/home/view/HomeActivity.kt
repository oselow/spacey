package com.example.spacey.home.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.example.spacey.BuildConfig
import com.example.spacey.R
import com.example.spacey.databinding.ActivityHomeBinding
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.spacey.apiFeat.view.ApiActivity
import com.example.spacey.apiFeat.viewModel.ApiViewModel
import com.example.spacey.apiFeat.viewModel.AppVersionViewModel
import com.example.spacey.firebase.view.FirebaseLoginActivity


class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding

    private lateinit var appVersionViewModel: AppVersionViewModel

    private lateinit var firstPerson: TextView
    private lateinit var secondPerson: TextView
    private lateinit var androidVersion: TextView
    private lateinit var launchLoginActivity: Button
    private lateinit var launchApiActivity: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firstPerson = binding.homeTextViewFirstPerson
        secondPerson = binding.homeTextViewSecondPerson
        androidVersion = binding.homeTextViewAndroidVersionName
        launchLoginActivity = binding.homeButtonLaunchLoginFeat
        launchApiActivity = binding.homeButtonLaunchSecondFeat

        appVersionViewModel = ViewModelProvider(this)[AppVersionViewModel::class.java]

        launchLoginActivity.setOnClickListener { launchLoginActivity() }
        launchApiActivity.setOnClickListener {lauchApiActivity() }

        val title = getString(R.string.home_welcome) + " " + getString(R.string.app_name)
        binding.homeTextViewWelcome.text = title

        val firstPersonName = getString(R.string.firstname1) + " " + getString(R.string.lastname1)
        val secondPersonName = getString(R.string.firstname2) + " " + getString(R.string.lastname2)
        firstPerson.text = firstPersonName
        secondPerson.text = secondPersonName


        val versionCode = appVersionViewModel.getAppVersion()
        androidVersion.text = String.format(getString(R.string.dynamic_version), versionCode);

        if (intent.extras != null) {
            Log.d("tutu", intent.extras.toString())
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!![key]
                Toast.makeText(this, "Key: $key Value: $value", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun launchLoginActivity(){
        val intent = Intent(this, FirebaseLoginActivity::class.java)
        startActivity(intent)
    }

    private fun lauchApiActivity(){
        //todo
        val intent = Intent(this, ApiActivity::class.java)
        startActivity(intent)
    }
}