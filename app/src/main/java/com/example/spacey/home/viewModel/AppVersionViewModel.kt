package com.example.spacey.apiFeat.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.example.spacey.apiFeat.model.AnimalRoom
import com.example.spacey.apiFeat.model.AnimalUi
import com.example.spacey.apiFeat.model.AvatarRoom
import com.example.spacey.apiFeat.model.AvatarUi
import com.example.spacey.apiFeat.repository.AnimalQuoteRepository
import com.example.spacey.apiFeat.repository.AvatarRepository
import com.example.spacey.home.repository.AppVersionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AppVersionViewModel : ViewModel() {

    private val mAppVersionRepository: AppVersionRepository by lazy { AppVersionRepository() }

    fun getAppVersion(): String {
        return mAppVersionRepository.getAppVersion()
    }
}