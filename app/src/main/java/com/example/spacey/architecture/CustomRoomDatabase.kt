package com.example.spacey.architecture

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.spacey.apiFeat.dao.AnimalDao
import com.example.spacey.apiFeat.dao.AvatarDao
import com.example.spacey.apiFeat.model.AnimalRoom
import com.example.spacey.apiFeat.model.AvatarRoom

@Database(
    entities = [
        AvatarRoom::class,
        AnimalRoom::class
    ],
    version = 12,
    exportSchema = false
)
abstract class CustomRoomDatabase : RoomDatabase() {
    abstract fun avatarDao() : AvatarDao
    abstract fun animalDao(): AnimalDao
}
