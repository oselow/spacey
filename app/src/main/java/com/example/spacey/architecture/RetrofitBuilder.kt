import com.example.spacey.apiFeat.remote.AnimalEndPoint
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
        private val avatarRetrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://random-data-api.com/api/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
        .build()

        private val animalRetrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://cataas.com/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
            .build()

    fun getAvatar(): AvatarEndpoint = avatarRetrofit.create(AvatarEndpoint::class.java)
    fun getAnimal(): AnimalEndPoint = animalRetrofit.create(AnimalEndPoint::class.java)
}
